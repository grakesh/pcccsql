CREATE DATABASE  IF NOT EXISTS `pcccdb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pcccdb`;
-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: pcccdb
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Bookings`
--

DROP TABLE IF EXISTS `Bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bookings` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `PlanId` int(11) NOT NULL,
  `BookingDate` datetime NOT NULL,
  `PaymentTypeId` int(11) NOT NULL,
  `TransactionDate` datetime DEFAULT NULL,
  `PaymentStatus` int(11) NOT NULL,
  `PaymentAmount` decimal(10,2) NOT NULL,
  `TransactionId` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_Bookings_1_idx` (`PlanId`),
  KEY `fk_Bookings_2_idx` (`UserId`),
  CONSTRAINT `fk_Bookings_1` FOREIGN KEY (`PlanId`) REFERENCES `Plan` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Bookings_2` FOREIGN KEY (`UserId`) REFERENCES `User` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Bookings`
--

LOCK TABLES `Bookings` WRITE;
/*!40000 ALTER TABLE `Bookings` DISABLE KEYS */;
INSERT INTO `Bookings` VALUES (1,9,1,'2018-10-16 19:05:29',1,'2018-10-16 19:05:29',2,240.00,NULL),(2,9,4,'2018-10-16 19:10:52',1,'2018-10-16 19:10:52',2,350.00,NULL),(3,9,3,'2018-10-16 19:17:38',1,'2018-10-16 19:17:38',2,295.00,'ch_1DLx6fElM476F9EZCQLBi6L4'),(4,9,2,'2018-10-16 19:21:03',1,'2018-10-16 19:21:03',2,150.00,'ch_1DLxA8ElM476F9EZYzPkOFrF'),(5,9,1,'2018-10-16 19:23:59',1,'2018-10-16 19:23:59',2,240.00,'ch_1DLxCyElM476F9EZjmZ4Xf6h'),(6,9,3,'2018-10-16 19:26:17',1,'2018-10-16 19:26:17',2,295.00,'ch_1DLxFBElM476F9EZ3QUgfJ7S'),(7,10,1,'2018-10-16 19:28:29',1,'2018-10-16 19:28:29',2,240.00,'ch_1DLxHMElM476F9EZjdfUbyom'),(8,9,1,'2018-10-16 19:29:35',2,NULL,1,240.00,NULL),(9,11,1,'2018-10-21 09:29:21',2,NULL,1,240.00,NULL),(10,11,3,'2018-10-21 09:31:51',2,NULL,1,295.00,NULL),(11,9,1,'2018-10-21 13:00:28',2,NULL,1,240.00,NULL),(12,9,1,'2018-10-21 13:03:46',2,NULL,1,240.00,NULL),(13,9,1,'2018-10-21 13:06:15',2,NULL,1,240.00,NULL),(14,9,1,'2018-10-21 13:08:16',2,NULL,1,240.00,NULL),(15,9,1,'2018-10-21 13:12:32',2,NULL,1,240.00,NULL),(16,9,1,'2018-10-21 13:15:37',1,'2018-10-21 13:15:37',2,240.00,'ch_1DNfqHElM476F9EZoQjUcXXG'),(17,9,1,'2018-10-21 21:08:07',2,NULL,1,240.00,NULL),(18,9,1,'2018-10-21 21:08:47',2,NULL,1,240.00,NULL),(19,9,1,'2018-10-21 21:09:05',2,NULL,1,240.00,NULL),(20,9,1,'2018-10-21 21:09:36',2,NULL,1,240.00,NULL),(21,9,1,'2018-10-21 21:09:44',2,NULL,1,240.00,NULL),(22,9,1,'2018-10-21 21:09:54',2,NULL,1,240.00,NULL),(23,9,1,'2018-10-21 21:10:26',2,NULL,1,240.00,NULL),(24,9,1,'2018-10-21 23:32:43',2,NULL,1,240.00,NULL),(25,9,1,'2018-10-27 16:56:23',2,NULL,1,240.00,NULL),(26,9,1,'2018-11-03 15:10:19',2,NULL,1,240.00,NULL),(27,9,1,'2018-11-03 15:17:43',2,NULL,1,240.00,NULL),(28,9,1,'2018-11-03 15:21:12',2,NULL,1,240.00,NULL),(29,9,1,'2018-11-03 15:21:37',2,NULL,1,240.00,NULL);
/*!40000 ALTER TABLE `Bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CustomerSetup`
--

DROP TABLE IF EXISTS `CustomerSetup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CustomerSetup` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CompanyName` varchar(100) NOT NULL,
  `Address` varchar(200) NOT NULL,
  `PostCode` varchar(15) NOT NULL,
  `ContactNo1` varchar(50) DEFAULT NULL,
  `ContactNo2` varchar(50) DEFAULT NULL,
  `Website` varchar(50) DEFAULT NULL,
  `Licence` varchar(100) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CustomerSetup`
--

LOCK TABLES `CustomerSetup` WRITE;
/*!40000 ALTER TABLE `CustomerSetup` DISABLE KEYS */;
INSERT INTO `CustomerSetup` VALUES (1,'PCCC','92-100 Darlaston Road, Walsall, West Midlands','WS2 9RE','01922 633 004','07756 690 395','http://www.aquacleaningsolutions.com',NULL,'2018-10-05 14:04:50',NULL);
/*!40000 ALTER TABLE `CustomerSetup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Feature`
--

DROP TABLE IF EXISTS `Feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Feature` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(500) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `IsActive` bit(1) NOT NULL,
  `Colour` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Feature`
--

LOCK TABLES `Feature` WRITE;
/*!40000 ALTER TABLE `Feature` DISABLE KEYS */;
INSERT INTO `Feature` VALUES (1,'Snow Foam is applied to vehicle','2018-10-04 20:34:45',_binary '',NULL),(2,'Power wash bodywork including door shuts and wheel arches','2018-10-04 20:35:21',_binary '',NULL),(3,'Wheels cleaned throughly removing dirt and brake dust with iron removing non-acidic cleanser','2018-10-04 20:36:05',_binary '',NULL),(4,'Hand wash using two bucket Grit Guard system and a PH Neutral cleaning and conditioning agent','2018-10-05 13:30:44',_binary '',NULL),(5,'All surface contaminates and impurities removed from painted surfaces using clay bar','2018-10-05 13:31:30',_binary '',NULL),(6,'Rinse then Dry using microfiber towels','2018-10-05 13:32:01',_binary '',NULL),(7,'Remove Tar from paintwork','2018-10-05 13:32:17',_binary '',NULL),(8,'Paint Depth readings taken and recorded on file','2018-10-05 13:32:40',_binary '',NULL),(9,'3 - Stage Paint Correction process by machine','2018-10-05 13:33:10',_binary '','#ff9933'),(10,'Double coat of Premium Wax','2018-10-05 13:33:28',_binary '',NULL),(11,'Vacuum interior upholstery, mats and carpets, incl boot','2018-10-05 13:34:00',_binary '',NULL),(12,'Upholstery, door panels and carpets shampooed & dried','2018-10-05 13:34:57',_binary '',NULL),(13,'Leather cleaned and conditioned','2018-10-05 13:35:16',_binary '',NULL),(14,'Headlining cleaned','2018-10-05 13:35:40',_binary '',NULL),(15,'Bppt area shampooed','2018-10-05 13:35:53',_binary '',NULL),(16,'Fabric protector applied to carpets and upholstery','2018-10-05 13:36:20',_binary '',NULL),(17,'Leather protector applied if applicable','2018-10-05 13:36:50',_binary '',NULL),(18,'Windows and mirrors cleaned & glass coating appilied','2018-10-05 13:37:21',_binary '',NULL),(19,'Air vents and ashtrays cleaned','2018-10-05 13:37:37',_binary '',NULL),(20,'Interior fascia, plastics and interior door trim cleaned & treated','2018-10-05 13:38:11',_binary '',NULL),(21,'Single coat of long lasting Sealant on paint, leather, wheels, glass and upholstery','2018-10-05 13:38:51',_binary '',NULL),(22,'Alloy & Chrome polished & protected','2018-10-05 13:39:11',_binary '',NULL),(23,'Tyres, wheel arches & plastic trim sealant applied','2018-10-05 13:39:34',_binary '',NULL),(24,'Meguiars Lens, Treatment and Protection','2018-10-05 13:40:05',_binary '',NULL),(25,'A Gentle citrus pre-wash treatment is applied to the bodywork and wheel','2018-10-18 18:43:03',_binary '',NULL),(26,'Hand wash using two bucket Grit Guard system and a PH neutral shampoo','2018-10-18 18:43:41',_binary '',NULL),(27,'Through wheel clean with non-acidic cleaner','2018-10-18 18:44:00',_binary '',NULL),(28,'Tar & transportation film adhesive removed','2018-10-18 18:44:34',_binary '',NULL),(29,'All surface contaminants removed using a clay bar','2018-10-18 18:45:13',_binary '',NULL),(30,'Rinse then Dry using microfibre towels','2018-10-18 18:45:38',_binary '',NULL),(31,'Paintwork cleanser applied via hand or machine polish to provide a perfect base to aid sealant bonding','2018-10-18 18:46:24',_binary '',NULL),(32,'2 x layers of Sealant and 1 x layer of High Grade Carnauba Wax applied to Paintwork','2018-10-18 18:47:24',_binary '',NULL),(33,'Alloys & chrome polished and protected','2018-10-18 18:47:43',_binary '',NULL),(34,'Vacuum interior','2018-10-18 18:47:56',_binary '',NULL),(35,'Full interior protection','2018-10-18 18:48:17',_binary '',NULL),(36,'Fabric Protector applied to Carpets & Upholstery','2018-10-18 18:48:51',_binary '',NULL),(37,'Windows cleaned inside & out and \"Rainex\" applied to exterior glass','2018-10-18 18:49:29',_binary '',NULL),(38,'Interior trim and plastic cleaned','2018-10-18 18:49:50',_binary '',NULL),(39,'Tyres rubber and plastic trim treatment applied','2018-10-18 18:50:30',_binary '',NULL),(40,'Meguiars Lens Treatment and Protection','2018-10-18 18:50:54',_binary '',NULL),(41,'Autoglym Floor mats and Air freshener','2018-10-18 18:51:23',_binary '',NULL),(42,'Through wheel clean with non-acidic cleaner to remove dirt and brake duct','2018-10-18 18:52:30',_binary '',NULL),(43,'Tar removed','2018-10-18 18:52:42',_binary '',NULL),(44,'Clay bar treatment to remove any impurities or contamination from painted surface','2018-10-18 18:53:42',_binary '',NULL),(45,'1 - Stage Machine polish to remove swirl marks, imperfections and restore to a deep shine','2018-10-18 18:54:33',_binary '',NULL),(46,'2 x layers of premium wax applied to paintwork','2018-10-18 18:55:07',_binary '',NULL),(47,'Alloys & chrome polished','2018-10-18 18:55:29',_binary '',NULL),(48,'Exterior glass cleaned and polished','2018-10-18 18:55:47',_binary '',NULL),(49,'Tyre, wheel arches and plastic trim treatment applied','2018-10-18 18:56:24',_binary '',NULL);
/*!40000 ALTER TABLE `Feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PaymentType`
--

DROP TABLE IF EXISTS `PaymentType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PaymentType` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `MerchantIds` json DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PaymentType`
--

LOCK TABLES `PaymentType` WRITE;
/*!40000 ALTER TABLE `PaymentType` DISABLE KEYS */;
/*!40000 ALTER TABLE `PaymentType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Plan`
--

DROP TABLE IF EXISTS `Plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Plan` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Price` decimal(10,2) NOT NULL,
  `ServiceDuration` varchar(100) NOT NULL,
  `PriceText` varchar(50) NOT NULL,
  `IsActive` bit(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Name_UNIQUE` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Plan`
--

LOCK TABLES `Plan` WRITE;
/*!40000 ALTER TABLE `Plan` DISABLE KEYS */;
INSERT INTO `Plan` VALUES (1,'New Car Protection',240.00,'Approx time 1 day','From £240+',_binary ''),(2,'Exterior Enhancement',150.00,'Approx time 1 day','From £150+',_binary ''),(3,'Paintwork Correction',295.00,'Approx time 1 day','From £295+',_binary ''),(4,'Full Correction & Protection',350.00,'Approx time 2 days','From £350+',_binary '');
/*!40000 ALTER TABLE `Plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PlanFeature`
--

DROP TABLE IF EXISTS `PlanFeature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PlanFeature` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PlanId` int(11) NOT NULL,
  `FeatureId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `index4` (`PlanId`,`FeatureId`),
  KEY `fk_PlanFeature_1_idx` (`FeatureId`),
  KEY `fk_PlanFeature_2_idx` (`PlanId`),
  CONSTRAINT `fk_PlanFeature_1` FOREIGN KEY (`FeatureId`) REFERENCES `Feature` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_PlanFeature_2` FOREIGN KEY (`PlanId`) REFERENCES `Plan` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PlanFeature`
--

LOCK TABLES `PlanFeature` WRITE;
/*!40000 ALTER TABLE `PlanFeature` DISABLE KEYS */;
INSERT INTO `PlanFeature` VALUES (39,1,1),(40,1,2),(43,1,25),(44,1,26),(45,1,27),(46,1,28),(47,1,29),(48,1,30),(49,1,31),(50,1,32),(51,1,33),(52,1,34),(53,1,35),(54,1,36),(55,1,37),(56,1,38),(57,1,39),(58,1,40),(59,1,41),(41,2,1),(42,2,2),(73,2,25),(70,2,26),(75,2,27),(71,2,30),(68,2,40),(69,2,41),(60,2,42),(61,2,43),(62,2,44),(63,2,45),(64,2,46),(65,2,47),(66,2,48),(67,2,49),(27,3,1),(28,3,2),(29,3,3),(30,3,4),(31,3,5),(32,3,6),(33,3,7),(34,3,8),(35,3,9),(36,3,10),(38,3,22),(37,3,23),(1,4,1),(3,4,2),(4,4,3),(6,4,4),(7,4,5),(8,4,6),(9,4,7),(10,4,8),(11,4,9),(12,4,10),(13,4,11),(14,4,12),(15,4,13),(16,4,14),(17,4,15),(18,4,16),(19,4,17),(20,4,18),(21,4,19),(22,4,20),(23,4,21),(24,4,22),(25,4,23),(26,4,24);
/*!40000 ALTER TABLE `PlanFeature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Address` varchar(500) NOT NULL,
  `PostCode` varchar(20) NOT NULL,
  `ContactNo` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `IsActive` bit(1) DEFAULT NULL,
  `PromotionOptIn` bit(1) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Email_UNIQUE` (`Email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'Rakesh Gupta','Redditch','b97 4be','099999999','rake@hw.com',_binary '',NULL,'2018-10-15 20:48:13'),(7,'Rakesh Gupta','Redditch','b97 4be','099999999','rake2@hw11.com',_binary '',NULL,'2018-10-15 21:16:06'),(8,'','','','','',_binary '',NULL,'2018-10-15 22:40:02'),(9,'Rakesh','Reddditch','B97 4BE','0000000000','rakesh@howbitswork.com',_binary '',NULL,'2018-10-15 22:41:37'),(10,'Rakesh Gupta','Snowhill Road Barnt Green ENG United Kingdom GB','B45 7HT','07721033114','rakesh@gmail.com',_binary '',NULL,'2018-10-16 19:28:29'),(11,'Rakesh Gupta','Hagley road','B45 7HT','07721033114','rakesh.gupta@howbitswork.com',_binary '',NULL,'2018-10-21 09:29:21');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'pcccdb'
--
/*!50003 DROP PROCEDURE IF EXISTS `CreateBooking` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateBooking`(IN Name VARCHAR(100), IN Address VARCHAR(500), 
							IN PostCode VARCHAR(20), IN ContactNo VARCHAR(100),
                            IN Email VARCHAR(100), IN PlanId INT, IN PaymentTypeId INT,
                            IN TransactionId VARCHAR(500))
BEGIN

DECLARE user_id INT;
DECLARE transaction_id INT;
DECLARE PaymentAmount DECIMAL(10,2);

START TRANSACTION;

SELECT PRICE INTO PaymentAmount FROM Plan WHERE Id = PlanId;

SELECT Id INTO user_id FROM User WHERE User.Email = Email;

IF (user_id IS NULL) THEN
	
    INSERT INTO User(Name, Address, PostCode, ContactNo, Email, IsActive, CreatedDate)
    VALUES (Name, Address, PostCode, ContactNo, Email, true, NOW());
    
    SELECT LAST_INSERT_ID() INTO user_id;
    
END IF;

IF (PaymentTypeId = 1) THEN
	INSERT INTO `Bookings`(`UserId`,`PlanId`,`BookingDate`,`PaymentTypeId`,`TransactionDate`,`PaymentStatus`,
						`PaymentAmount`,`TransactionId`)
	VALUES (user_id, PlanId, NOW(), PaymentTypeId, NOW(), 2, PaymentAmount, TransactionId);
    
ELSE 

INSERT INTO `Bookings`(`UserId`,`PlanId`,`BookingDate`,`PaymentTypeId`, `PaymentStatus`,
						`PaymentAmount`,`TransactionId`)
	VALUES (user_id, PlanId, NOW(), PaymentTypeId, 1, PaymentAmount, NULL);

END IF;

SELECT  LAST_INSERT_ID() INTO transaction_id;

SELECT transaction_id;

COMMIT;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getPlanById` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getPlanById`(IN PlanId INT)
BEGIN

SELECT Id, Name, Price, ServiceDuration, PriceText 
FROM Plan P 
WHERE Id = PlanId AND IsActive = true;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getPlanFeatures` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getPlanFeatures`(IN PlanId INT)
BEGIN

IF (PlanId IS NULL) THEN
		SELECT P.Id AS PlanId, P.Name AS PlanName, P.Price, P.ServiceDuration, 
		P.PriceText, F.Name AS FeatureName, F.Colour AS FeatureColour 
		FROM PlanFeature PF
		JOIN Plan P ON P.Id = PF.PlanId
		JOIN Feature F ON F.Id = PF.FeatureId
		WHERE P.IsActive = true AND F.IsActive = true
		ORDER BY P.Id ASC;
ELSE
        SELECT P.Id AS PlanId, P.Name AS PlanName, P.Price, P.ServiceDuration, 
		P.PriceText, F.Name AS FeatureName, F.Colour AS FeatureColour 
		FROM PlanFeature PF
		JOIN Plan P ON P.Id = PF.PlanId
		JOIN Feature F ON F.Id = PF.FeatureId
		WHERE P.IsActive = true AND F.IsActive = true AND P.Id = PlanId
		ORDER BY P.Id ASC;
END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-30 19:51:03
